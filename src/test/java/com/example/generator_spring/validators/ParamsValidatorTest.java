package com.example.generator_spring.validators;

import com.example.generator_spring.structures.Range;
import com.example.generator_spring.structures.TransactionConfiguration;
import org.junit.Assert;
import org.junit.Test;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.Locale;

public class ParamsValidatorTest {
    @Test
    public void defaultData() {
        //given
        ParamsValidator uut = new ParamsValidator();
        LocalDate today = LocalDate.now();
        System.out.println(today);
        String currentDate = today.toString() + "T00:00:00.000-0100:" + today.toString() + "T23:59:59.999-0100";
        Method method;
        String result = null;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateDate", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"" );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(result.equals(currentDate));
    }

    @Test
    public void acceptCorrectDate() {
        //given
        ParamsValidator uut = new ParamsValidator();
        LocalDate thatDate = LocalDate.parse("2018-06-14");
        String currentDate = thatDate.toString() + "T00:00:00.000-0100:" + thatDate.toString() + "T23:59:59.999-0100";
        Method method;
        String result = null;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateDate", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"2018-06-14T00:00:00.000-0100:2018-06-14T23:59:59.999-0100" );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result.equals(currentDate));
    }

    @Test
    public void correctRange() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Range<Integer> expected = new Range<>(7,9);
        Method method;
        Class[] params = new Class[3];
        params[0] = String.class; params[1] = String.class; params[2] = Range.class;
        Range<Integer> result = null;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateRangeAndGetRangeObject", params);
            method.setAccessible(true);
            result = (Range<Integer>) method.invoke(uut,"7:9", "cos", new Range<>(1, 5) );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result.getFrom() == expected.getFrom() && result.getTo() == result.getTo());
    }

    @Test
    public void wrongRange() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Range<Integer> expected = new Range<>(1,5);
        Method method;
        Class[] params = new Class[3];
        params[0] = String.class; params[1] = String.class; params[2] = Range.class;
        Range<Integer> result = null;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateRangeAndGetRangeObject", params);
            method.setAccessible(true);
            result = (Range<Integer>) method.invoke(uut,"11:9", "cos", new Range<>(1, 5) );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result.getFrom() == expected.getFrom() && result.getTo() == result.getTo());
    }

    @Test
    public void emptyRangeString() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Range<Integer> expected = new Range<>(1,5);
        Method method;
        Class[] params = new Class[3];
        params[0] = String.class; params[1] = String.class; params[2] = Range.class;
        Range<Integer> result = null;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateRangeAndGetRangeObject", params);
            method.setAccessible(true);
            result = (Range<Integer>) method.invoke(uut,"", "cos", new Range<>(1, 5) );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result.getFrom() == expected.getFrom() && result.getTo() == result.getTo());
    }

    @Test
    public void defaultNumberOfTrans() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Method method;
        int result = 0;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateNumberOfTrans", String.class);
            method.setAccessible(true);
            result = (int) method.invoke(uut, "" );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result == 100);
    }

    @Test
    public void correctNumberOfTrans() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Method method;
        int result = 0;
        try {
            //when
            method = ParamsValidator.class.getDeclaredMethod("validateNumberOfTrans", String.class);
            method.setAccessible(true);
            result = (int) method.invoke(uut, "5" );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //given
        Assert.assertTrue(result == 5);
    }

    @Test
    public void validateNormalRequest() {
        //given
        ParamsValidator uut = new ParamsValidator();
        Range<Integer> cust = new Range<>(2, 3);
        Range<Integer> items = new Range<>(1, 5);
        Range<Integer> qua = new Range<>(1, 3);

        //when
        TransactionConfiguration transactionConfiguration = uut.validateInputAndGetTransactionConfiguration(
                "2:3", "", "1:5", "1:3", "100");

        //then
        Assert.assertTrue(transactionConfiguration.getNumberOfTrans() == 100);
        Assert.assertTrue(transactionConfiguration.getRangeOfCustomerId().getFrom() == 2);

    }
}
