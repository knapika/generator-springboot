package com.example.generator_spring.readers;

import com.example.generator_spring.readers.ItemsReader;
import com.example.generator_spring.structures.Item;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.List;

public class ItemsReaderTest {
    @Test
    public void getItemsFromCorrectUrl() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method = null;
        String result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("getItemsString", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"https://csv-items-generator.herokuapp.com/items");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.length() > 0);
    }

    @Test
    public void urlWithoutItems() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method = null;
        String result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("getItemsString", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"https://csv-items-generator.herokuapp.com/item");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.length() == 0);
    }

    @Test
    public void transformEmptyStringToArray() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method = null;
        String[] result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("transformStringToArrayItems", String.class);
            method.setAccessible(true);
            result = (String[]) method.invoke(uut,"");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.length == 1);
    }

    @Test
    public void transformCorrectStringToArray() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method = null;
        String[] result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("transformStringToArrayItems", String.class);
            method.setAccessible(true);
            result = (String[]) method.invoke(uut,"[{\"name\": \"woda\"}, {\"name\": \"cola\"}]");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.length == 2);
    }

    @Test
    public void transformEmptyCell() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method = null;
        String result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("processOneCell", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.equals(""));
    }

    @Test
    public void transformCorrectCell() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method;
        String result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("processOneCell", String.class);
            method.setAccessible(true);
            result = (String) method.invoke(uut,"[{\"name\": \"woda\"}");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.equals("[\"name\": \"woda\"}"));
    }

    @Test
    public void createItemFromEmptyString() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method;
        Item result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("createItem", String.class);
            method.setAccessible(true);
            result = (Item) method.invoke(uut,"");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.getPrice() == 0 && result.getName().equals(""));
    }

    @Test
    public void createItemFromCorrectString() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method;
        Item result = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("createItem", String.class);
            method.setAccessible(true);
            result = (Item) method.invoke(uut,"woda, 5.0");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.getPrice() == 5.0 && result.getName().equals("woda"));
    }

    @Test
    public void createItemsFromTwoStrings() {
        //given
        ItemsReader uut = new ItemsReader();
        Method method;
        Item result[] = null;
        try {
            //when
            method = ItemsReader.class.getDeclaredMethod("getItemsList", String.class);
            method.setAccessible(true);
            result = (Item[]) method.invoke(uut,"https://csv-items-generator.herokuapp.com/items" );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //then
        Assert.assertTrue(result.length == 9);
    }
}
