package com.example.generator_spring.structures;

import org.junit.Assert;
import org.junit.Test;

public class RangeTest {
    @Test
    public void getFrom() {
        //given
        Range<Integer> uut = new Range(2,4);

        //then
        Assert.assertTrue(uut.getFrom() == 2);
    }

    @Test
    public void getTo() {
        //given
        Range<Integer> uut = new Range(2,4);

        //then
        Assert.assertTrue(uut.getTo() == 4);
    }
}
