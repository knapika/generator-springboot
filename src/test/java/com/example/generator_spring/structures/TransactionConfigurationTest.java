package com.example.generator_spring.structures;

import org.junit.Assert;
import org.junit.Test;

public class TransactionConfigurationTest {
    @Test
    public void getCustomerId() {
        //given
        Range<Integer> customerIds = new Range(1,20);
        Range<Integer> itemsCount = new Range(5,15);
        Range<Integer> itemsQua = new Range(1,30);
        TransactionConfiguration uut = new TransactionConfiguration(customerIds,
                "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100", itemsCount, itemsQua,
                1000);

        //when

        //then
        Assert.assertEquals(uut.getRangeOfCustomerId(), customerIds);
    }

    @Test
    public void getDate() {
        //given
        Range<Integer> customerIds = new Range(1,20);
        Range<Integer> itemsCount = new Range(5,15);
        Range<Integer> itemsQua = new Range(1,30);
        String date = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100";
        TransactionConfiguration uut = new TransactionConfiguration(customerIds, date, itemsCount, itemsQua,
                1000);

        //when

        //then
        Assert.assertEquals(uut.getRangeOfDate(), date);
    }

    @Test
    public void getNumberOfItems() {
        //given
        Range<Integer> customerIds = new Range(1,20);
        Range<Integer> itemsCount = new Range(5,15);
        Range<Integer> itemsQua = new Range(1,30);
        String date = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100";
        TransactionConfiguration uut = new TransactionConfiguration(customerIds, date, itemsCount, itemsQua,
                1000);

        //when

        //then
        Assert.assertEquals(uut.getRangeOfnumberOfItems(), itemsCount);
    }

    @Test
    public void getRangeOfQuantities() {
        //given
        Range<Integer> customerIds = new Range(1,20);
        Range<Integer> itemsCount = new Range(5,15);
        Range<Integer> itemsQua = new Range(1,30);
        String date = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100";
        TransactionConfiguration uut = new TransactionConfiguration(customerIds, date, itemsCount, itemsQua,
                1000);


        //when

        //then
        Assert.assertEquals(uut.getRangeOfQuantities(), itemsQua);
    }

    @Test
    public void getNumberOfTrans() {
        //given
        Range<Integer> customerIds = new Range(1,20);
        Range<Integer> itemsCount = new Range(5,15);
        Range<Integer> itemsQua = new Range(1,30);
        String date = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100";
        TransactionConfiguration uut = new TransactionConfiguration(customerIds, date, itemsCount, itemsQua,
                1000);


        //when

        //then
        Assert.assertEquals(uut.getNumberOfTrans(), 1000);
    }
}
