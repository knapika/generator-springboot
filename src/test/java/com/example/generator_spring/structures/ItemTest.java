package com.example.generator_spring.structures;

import org.junit.Assert;
import org.junit.Test;

public class ItemTest {
    @Test
    public void getName() {
        //given
        Item uut = new Item("Bułka", 2.0);

        //when

        //then
        Assert.assertTrue(uut.getName().equals("Bułka"));
    }

    @Test
    public void getPrice() {
        //given
        Item uut = new Item("Bułka", 2.0);

        //when

        //then
        Assert.assertTrue(uut.getPrice() == 2.0);
    }

    @Test
    public void getQunt() {
        //given
        Item uut = new Item("Bułka", 2,2.0);

        //when

        //then
        Assert.assertTrue(uut.getQuantity() == 2);
    }
}
