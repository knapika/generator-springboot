package com.example.generator_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  GeneratorSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorSpringApplication.class, args);
	}
}
