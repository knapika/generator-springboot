package com.example.generator_spring.readers;

import com.example.generator_spring.generators.TransactionsGenerator;
import com.example.generator_spring.structures.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

@Component
public class ItemsReader {
    private final Logger ItemsReaderLogger = LogManager.getLogger(ItemsReader.class);

    public Item[] getItemsList(String pathToData) {
        ItemsReaderLogger.info("Start items downloading!");
        String itemsString = getItemsString(pathToData);
        String[] arrayWithItemsString = transformStringToArrayItems(itemsString);
        Item[] itemsArray = createItemsFromStringsArray(arrayWithItemsString);
        return itemsArray;
    }

    private Item[] createItemsFromStringsArray(String[] arrayWithItemsString) {
        ItemsReaderLogger.info("Start creating Item Obcjects from strings");
        Item[] items = new Item[arrayWithItemsString.length];
        for(int i = 0; i < items.length; i++) {
            items[i] = createItem(arrayWithItemsString[i]);
            ItemsReaderLogger.info("Item " + i + " created");
        }
        return items;
    }

    private Item createItem(String s) {
        s = s.replace("name", "");
        s = s.replace("price", "");
        s = s.replace(":", "");
        s = s.replace("\"","");
        String[] itemParam = s.split(",");
        if(itemParam.length == 2) {
            return new Item(itemParam[0], Double.valueOf(itemParam[1]));
        } else {
            return new Item("", 0.0);
        }

    }

    private String[] transformStringToArrayItems(String itemsString) {
        ItemsReaderLogger.info("Split items string to array!");
        itemsString = itemsString.replace("[","");
        itemsString = itemsString.replace("]","");
        String[] array = itemsString.split("}");
        for(int i = 0; i < array.length; i++) {
            array[i] = processOneCell(array[i]);
            ItemsReaderLogger.info("Cell " + i + "was processed!");
        }
        return array;
    }

    private String processOneCell(String s) {
        s = s.replace(",{", "");
        s = s.replace("{","");
        return s;
    }

    private String getItemsString(String pathToData) {
        URL url = null;
        String output = "";
        try {
            url = new URL(pathToData);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                ItemsReaderLogger.warn("Failed : HTTP error code : " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            output = br.readLine();
            conn.disconnect();
            ItemsReaderLogger.info("All items were processed!");

        } catch (MalformedURLException e) {
            ItemsReaderLogger.error("Problem with items reading " + e.getMessage());
        } catch (ProtocolException e) {
            ItemsReaderLogger.error("Problem with items reading " + e.getMessage());
        } catch (IOException e) {
            ItemsReaderLogger.error("Problem with items reading " + e.getMessage());
        }
        return output;
    }
}

