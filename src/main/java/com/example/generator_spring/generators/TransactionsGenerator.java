package com.example.generator_spring.generators;

import com.example.generator_spring.readers.ItemsReader;
import com.example.generator_spring.structures.Item;
import com.example.generator_spring.structures.Transaction;
import com.example.generator_spring.structures.TransactionConfiguration;
import com.example.generator_spring.validators.ParamsValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Controller
public class TransactionsGenerator {
    @Autowired
    private ItemsReader itemsReader;
    @Autowired
    private ParamsValidator validator;
    @Autowired
    private RandomsGenerator randomsGenerator;

    private final Logger TransactionGeneratorLogger = LogManager.getLogger(TransactionsGenerator.class);

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Transaction> answerForRequestJSON(@RequestParam(value = "customersId", required = false, defaultValue = "1:20") String customersId,
                                    @RequestParam(value = "dateRange", required = false, defaultValue = "") String dateRange,
                                    @RequestParam(value = "itemsCount", required = false, defaultValue = "1:5") String itemsCount,
                                    @RequestParam(value = "itemsQuantity", required = false, defaultValue = "1:5") String itemsQuantity,
                                    @RequestParam(value = "eventsCount", required = false, defaultValue = "100") String eventsCount) {

        TransactionConfiguration transConfig = validator.validateInputAndGetTransactionConfiguration(customersId,
                dateRange, itemsCount, itemsQuantity, eventsCount);

        Item[] items = itemsReader.getItemsList("https://csv-items-generator.herokuapp.com/items");

        List<Transaction> transactions = generateTransactions(transConfig, items);
        return transactions;
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public List<Transaction> answerForRequestXML(@RequestParam(value = "customersId", required = false, defaultValue = "1:20") String customersId,
                                                  @RequestParam(value = "dateRange", required = false, defaultValue = "") String dateRange,
                                                  @RequestParam(value = "itemsCount", required = false, defaultValue = "1:5") String itemsCount,
                                                  @RequestParam(value = "itemsQuantity", required = false, defaultValue = "1:5") String itemsQuantity,
                                                  @RequestParam(value = "eventsCount", required = false, defaultValue = "100") String eventsCount) {

        TransactionConfiguration transConfig = validator.validateInputAndGetTransactionConfiguration(customersId,
                dateRange, itemsCount, itemsQuantity, eventsCount);

        Item[] items = itemsReader.getItemsList("https://csv-items-generator.herokuapp.com/items");

        List<Transaction> transactions = generateTransactions(transConfig, items);
        return transactions;
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = "application/yaml")
    @ResponseBody
    public String answerForRequestYAML(@RequestParam(value = "customersId", required = false, defaultValue = "1:20") String customersId,
                                                 @RequestParam(value = "dateRange", required = false, defaultValue = "") String dateRange,
                                                 @RequestParam(value = "itemsCount", required = false, defaultValue = "1:5") String itemsCount,
                                                 @RequestParam(value = "itemsQuantity", required = false, defaultValue = "1:5") String itemsQuantity,
                                                 @RequestParam(value = "eventsCount", required = false, defaultValue = "100") String eventsCount) {

        TransactionConfiguration transConfig = validator.validateInputAndGetTransactionConfiguration(customersId,
                dateRange, itemsCount, itemsQuantity, eventsCount);

        Item[] items = itemsReader.getItemsList("https://csv-items-generator.herokuapp.com/items");

        List<Transaction> transactions = generateTransactions(transConfig, items);
        return getYAMLRequest(transactions);
    }

    private String getYAMLRequest(List<Transaction> transactions){
        TransactionGeneratorLogger.info("YAML format was selected!");
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        StringBuilder sb = new StringBuilder("");
        for(Transaction transaction : transactions) {
            try {
                sb.append(mapper.writeValueAsString(transaction)).append("\n");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private List<Transaction> generateTransactions(TransactionConfiguration transactionConfiguration, Item[] items) {
        TransactionGeneratorLogger.info("Start generating process!");
        List<Transaction> listOfTransactions = new LinkedList<>();
        int numberOfTrans = Integer.valueOf(transactionConfiguration.getNumberOfTrans());

        for(int i = 0; i < numberOfTrans; i++) {
            TransactionGeneratorLogger.info("Get random customerId");
            int customerId = randomsGenerator.getInteger(transactionConfiguration.getRangeOfCustomerId().getFrom(),
                    transactionConfiguration.getRangeOfCustomerId().getTo());
            TransactionGeneratorLogger.info("Get random itemsCount");
            int itemsCount = randomsGenerator.getInteger(transactionConfiguration.getRangeOfnumberOfItems().getFrom(),
                    transactionConfiguration.getRangeOfnumberOfItems().getTo());
            LocalDateTime timestemp = randomsGenerator.getTimestamp(transactionConfiguration.getRangeOfDate());
            Item[] itemsInTran = new Item[itemsCount];
            double sum = 0;
            for(int j = 0; j < itemsCount; j++){
                TransactionGeneratorLogger.info("Get random structures.Item");
                int randomItemIndex = randomsGenerator.getInteger(0, items.length);
                TransactionGeneratorLogger.info("Get random quantity");
                int quantity = randomsGenerator.getInteger(transactionConfiguration.getRangeOfQuantities().getFrom(),
                        transactionConfiguration.getRangeOfQuantities().getTo());
                TransactionGeneratorLogger.info("Create new structures.Item in transaction");
                itemsInTran[j] =new Item(items[randomItemIndex].getName(), quantity,
                        items[randomItemIndex].getPrice());
                sum += itemsInTran[j].getPrice() * quantity;
            }
            TransactionGeneratorLogger.info("Create new transaction and add to list");
            Transaction tran = new Transaction(i, customerId, timestemp, itemsInTran, sum);
            listOfTransactions.add(tran);
        }
        return listOfTransactions;
    }
}
