package com.example.generator_spring.structures;

//import writers.IWriter;

public class TransactionConfiguration {
    private final Range<Integer> rangeOfCustomerId;
    private final String rangeOfDate;
    private final Range<Integer> rangeOfnumberOfItems;
    private final Range<Integer> rangeOfQuantities;
    private final int numberOfTrans;

    public TransactionConfiguration(Range<Integer> rangeOfCustomerId, String rangeOfDate,
                                    Range<Integer> rangeOfnumberOfItems, Range<Integer> rangeOfQuantities,
                                    int numberOfTrans) {
        this.rangeOfCustomerId = rangeOfCustomerId;
        this.rangeOfDate = rangeOfDate;
        this.rangeOfnumberOfItems = rangeOfnumberOfItems;
        this.rangeOfQuantities = rangeOfQuantities;
        this.numberOfTrans = numberOfTrans;
    }

    public Range<Integer> getRangeOfCustomerId() {
        return rangeOfCustomerId;
    }

    public String getRangeOfDate() {
        return rangeOfDate;
    }

    public Range<Integer> getRangeOfnumberOfItems() {
        return rangeOfnumberOfItems;
    }

    public Range<Integer> getRangeOfQuantities() {
        return rangeOfQuantities;
    }

    public int getNumberOfTrans() {
        return numberOfTrans;
    }
}


