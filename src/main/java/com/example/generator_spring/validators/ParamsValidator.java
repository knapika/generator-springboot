package com.example.generator_spring.validators;

import com.example.generator_spring.generators.TransactionsGenerator;
import com.example.generator_spring.structures.Range;
import com.example.generator_spring.structures.TransactionConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Component
public class ParamsValidator {
    private final Logger ParamsValidatorLogger = LogManager.getLogger(ParamsValidator.class);

    public TransactionConfiguration validateInputAndGetTransactionConfiguration(String rangeOfCustomerId,
                                                                                String rangeOfDate,
                                                                                String rangeOfnumberOfItems,
                                                                                String rangeOfQuantities,
                                                                                String numberOfTrans) {
        Range<Integer> rangeForCustomerIDs = validateRangeAndGetRangeObject(rangeOfCustomerId, "CustomerIds",
                new Range<>(1,20));
        Range<Integer> rangeForItems = validateRangeAndGetRangeObject(rangeOfnumberOfItems, "ItemsRange",
                new Range<Integer>(1, 5));
        Range<Integer> rangeForQuantities = validateRangeAndGetRangeObject(rangeOfQuantities, "QuantitiesRange",
                new Range<>(1, 5));
        String dateRange = validateDate(rangeOfDate);
        int transCount = validateNumberOfTrans(numberOfTrans);

        return new TransactionConfiguration(rangeForCustomerIDs, dateRange, rangeForItems, rangeForQuantities,
                transCount);
    }

    private Range<Integer> validateRangeAndGetRangeObject(String range, String paramName, Range<Integer> defaultRange) {
        Range<Integer> result;
        if(range.length() > 0) {
            String[] tempString = range.split(":");
            result = new Range<>(Integer.valueOf(tempString[0]), Integer.valueOf(tempString[1]));
            if(result.getFrom() <= result.getTo()) {
                ParamsValidatorLogger.debug("Correct range for " + paramName);
                return result;
            } else {
                ParamsValidatorLogger.debug("Wrong range for " + paramName + " used default!");
                return defaultRange;
            }
        }
        return defaultRange;
    }

    private String validateDate(String range) {
        String dateResult = "";
        if(range.length() > 0) {
            dateResult = range;
            dateResult = dateResult.replace("\"","");
        } else {
            dateResult = setDefaultDate();
        }
        return dateResult;
    }

    private int validateNumberOfTrans(String numberOfTrans) {
        int result;
        if(numberOfTrans.length() > 0) {
            result = Integer.valueOf(numberOfTrans);
        } else {
            result = 100;
            ParamsValidatorLogger.debug("Used default amount for trans number");
        }
        return result;
    }

    private String setDefaultDate() {
        ParamsValidatorLogger.info("Set default date");
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        String todayStart = LocalDate.now().toString() + "T" + LocalTime.of(0,0,0,0)
                .format(formatter) + "-0100";
        String todayEnd = LocalDate.now().toString() + "T" + LocalTime.of(23,59,59,999999999)
                .format(formatter) + "-0100";
        return todayStart + ":" + todayEnd.toString();
    }
}
